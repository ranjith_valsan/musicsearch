//
//  BaseAPIManager.swift
//  Testing
//
//  Created by mobii on 12/12/18.
//  Copyright © 2018 Jaba. All rights reserved.
//
import Alamofire
import Reachability
import SwiftyJSON
import Foundation

public enum SearchType:String {
    case album = "album"
    case artist = "artist"
    case track = "track"
}

class BaseAPIManager {
    
    // MARK: -
    // MARK: Main POST function
    
    class func post (method:HTTPMethod = .post, url:String!, parameters:Parameters?,needUserToken:Bool, completion:@escaping CompletionBlock)
    {
        //
        guard let connection = AppDelegate.appdelegate.reachability?.connection else
        {
            completion(false, [:])
            return
        }
        guard  connection != .none else {
            completion(false, [:])
            return
        }
        
        let queue = DispatchQueue(label: "com.lymo.rider-queue", qos: .utility, attributes: [.concurrent])
        
        
        let manager = Alamofire.SessionManager.default
        
        
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        manager.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers:headers).responseJSON( queue: queue, options: .allowFragments,completionHandler: { response in
            if let status = response.response?.statusCode {
                switch(status){
                case 200:
                    if let result = response.result.value {
                        let resultDictionary = result as! [String : Any]
                        let resultJSON = JSON(resultDictionary)
                        completion(true, resultJSON)
                    }
                    
                default:
                    
                    DispatchQueue.main.async {
                        print("error with response status: \(status)")
                        completion(false, [:])
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    completion(false, [:])
                }
            }
            
        })
        
    }
    
}
