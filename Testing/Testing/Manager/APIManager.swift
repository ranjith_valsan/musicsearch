//
//  APIManager.swift
//  Testing
//
//  Created by mobii on 12/12/18.
//  Copyright © 2018 Jaba. All rights reserved.
//

import Foundation
import Alamofire
import Reachability
import SwiftyJSON


typealias CompletionBlock = (Bool, JSON) -> Void
let pageLimit = 20
class APIManager:BaseAPIManager {
    
    static func fetchSearchResult(searchString:String, page:Int,type:SearchType, completion:@escaping CompletionBlock) {
        var url:String = AppDelegate.appdelegate.webserviceBaseUrl + "method=\(type.rawValue).search&\(type.rawValue)=\(searchString)&api_key=\(AppDelegate.appdelegate.apiKey)&format=json&limit=\(pageLimit)&page=\(page)"
        url = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        post(method: .get, url: url, parameters: nil,  needUserToken: true, completion: completion)
    }
    
    
    static func fetchInfo(item:Item, completion:@escaping CompletionBlock) {
        var url:String?
        if item.category == .artist{
            url = AppDelegate.appdelegate.webserviceBaseUrl + "method=\(item.category.rawValue).getinfo&api_key=\(AppDelegate.appdelegate.apiKey)&\(item.category.rawValue)=\(item.name ?? "")&format=json"
        }else{
            url = AppDelegate.appdelegate.webserviceBaseUrl + "method=\(item.category.rawValue).getinfo&api_key=\(AppDelegate.appdelegate.apiKey)&\(item.category.rawValue)=\(item.name ?? "")&format=json&artist=\(item.artist ?? "")"
        }
        url = url?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        post(method: .get, url: url, parameters: nil,  needUserToken: true, completion: completion)
    }
    
}
