//
//  ItemTableViewCell.swift
//  Testing
//
//  Created by mobii on 18/12/18.
//  Copyright © 2018 Jaba. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var artWorkImageView: UIImageView!
    var item:Item? {
        didSet{
            self.titleLabel.text = item?.name
            if (item?.category == .track || item?.category == .album){
                self.subTitleLabel.text = "Artist: " + (item?.artist ?? "")
            }else {
                self.subTitleLabel.text = ""
            }
            guard let imageUrl = item?.artWorkUrl else { return }
            self.artWorkImageView.sd_setImage(with:  URL(string : imageUrl), placeholderImage: UIImage(named: item?.category.rawValue ?? "album"), options: .progressiveDownload, completed: nil)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
