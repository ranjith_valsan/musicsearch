//
//  UIViewController+Extension.swift
//  Testing
//
//  Created by mobii on 18/12/18.
//  Copyright © 2018 Jaba. All rights reserved.
//

import Foundation

import NVActivityIndicatorView

extension UIViewController {
    func startActivityIndicator() {
        DispatchQueue.main.async {
            NVActivityIndicatorView.DEFAULT_TYPE = .ballScale
            NVActivityIndicatorView.DEFAULT_COLOR = UIColor.green
            NVActivityIndicatorView.DEFAULT_PADDING = 10
            let activityData = ActivityData()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        }
    }
    
    func stopActivityIndicator() {
        DispatchQueue.main.async {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        }
    }


}
