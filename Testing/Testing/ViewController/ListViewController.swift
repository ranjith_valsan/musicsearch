//
//  ListViewController.swift
//  Testing
//
//  Created by Ranjith on 12/12/18.
//  Copyright © 2018 Jaba. All rights reserved.
//

import UIKit
import SDWebImage

typealias CompleteBlock = ([Item]?) -> Void

class ListViewController: UIViewController {
    @IBOutlet weak var listTableView: UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    var items = [Item]()
    var album = [Item]()
    var artist = [Item]()
    var track = [Item]()
    var albumPage = 1
    var artistPage = 1
    var trackPage = 1
    var seachType:SearchType = .album
    var loadMore = true
    var selectedItem:Item?
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewCell()
        setUpSearchController()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchController.isActive = true
    }
    
    // MARK: - APIs

    func fetchAlbum(searchFor:String,completion:@escaping CompleteBlock) {
        APIManager.fetchSearchResult(searchString: searchFor, page: albumPage,type: .album) { (success, response) in
            if success
            {
                let albums = response["results"]["albummatches"]["album"].arrayValue.map({Item.init(json:$0,searchCategory: .album)})
                self.loadMore = true
                
                completion(albums)
            }else{
                self.loadMore = false
                completion(nil)
            }
        }
    }
    
    func fetchArtist(searchFor:String,completion:@escaping CompleteBlock) {
        APIManager.fetchSearchResult(searchString: searchFor, page: artistPage,type: .artist) { (success, response) in
            if success
            {
                let artists = response["results"]["artistmatches"]["artist"].arrayValue.map({Item.init(json:$0,searchCategory: .artist)})
                self.loadMore = true
                completion(artists)
            }else{
                self.loadMore = false
                completion(nil)
            }
        }
    }
    
    func fetchTrack(searchFor:String,completion:@escaping CompleteBlock) {
        APIManager.fetchSearchResult(searchString: searchFor, page: trackPage,type: .track) { (success, response) in
            if success
            {
                let tracks = response["results"]["trackmatches"]["track"].arrayValue.map({Item.init(json:$0,searchCategory: .track)})
                self.loadMore = true
                completion(tracks)
            }else{
                self.loadMore = false
                completion(nil)
            }
        }
    }
    
    // MARK: - Private instance methods
    fileprivate func setUpSearchController () {
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Albums. Atrists or Tracks"
        searchController.searchBar.delegate = self
        navigationItem.searchController = searchController
        definesPresentationContext = true
        // Setup the Scope Bar
        searchController.searchBar.scopeButtonTitles = ["Album", "Artist", "Track"]
        searchController.searchBar.delegate = self
        searchController.searchBar.showsScopeBar = true
    }
    
    fileprivate func registerTableViewCell () {
        self.listTableView.isHidden = true
        let itemCell = UINib(nibName: "ItemTableViewCell", bundle: nil)
        self.listTableView.register(itemCell, forCellReuseIdentifier: "ItemTableViewCell")
        self.listTableView.estimatedRowHeight = 95
        self.listTableView.rowHeight = UITableView.automaticDimension
    }
    
    fileprivate func searchForContent(searchString:String, scope: String ) {
        if searchString.isEmpty{
            self.album.removeAll()
            self.artist.removeAll()
            self.track.removeAll()
            self.items.removeAll()
            self.listTableView.reloadData()
            self.listTableView.isHidden = true
            return
        }
        let radQueue = OperationQueue()
        let operation1 = BlockOperation {
            let group = DispatchGroup()
            group.enter()
            self.fetchAlbum(searchFor: searchString, completion: { (albums) in
                guard let albums = albums else { group.leave()
                    return }
                self.album = albums
                group.leave()
            })
            
            group.enter()
            self.fetchArtist(searchFor: searchString, completion: { (artists) in
                guard let artists = artists else { group.leave()
                    return}
                self.artist = artists
                group.leave()
            })
            
            group.enter()
            self.fetchTrack(searchFor: searchString, completion: { (tracks) in
                guard let tracks = tracks else { group.leave()
                    return}
                self.track = tracks
                group.leave()
            })
            group.wait()
        }
        
        let operation2 = BlockOperation {
            DispatchQueue.main.async {
                self.filterContentForSearchText()
            }
        }
        operation2.addDependency(operation1)
        radQueue.addOperation(operation1)
        radQueue.addOperation(operation2)
        
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText() {
        
        switch self.seachType {
        case .album:
            self.items = self.album
        case .artist:
            self.items = self.artist
        case .track:
            self.items = self.track
        }
        if items.count > 0{
            self.listTableView.isHidden = false
        }
        self.listTableView.reloadData()
    }
    
    func updateContentForSearchText(item:[Item]) {
        if items.count > 0{
            self.listTableView.isHidden = false
        }
        let indexes = (self.items.count - item.count ..< self.items.count)
            .map { IndexPath(row: $0, section: 0) }
        DispatchQueue.main.async {
            self.items.append(contentsOf:item)
            self.listTableView.beginUpdates()
            self.listTableView.insertRows(at: indexes, with: .none)
            self.listTableView.endUpdates()
        }
    }
    
     // MARK: - Navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetail"
        {
            let destinationVC = segue.destination as! DetailViewController
            destinationVC.item = selectedItem
        }
     }
    
}

extension ListViewController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        switch selectedScope {
        case 0:
            seachType = .album
        case 1:
            seachType = .artist
        case 2:
            seachType = .track
        default:
            break
        }
        if !listTableView.isHidden{
            DispatchQueue.main.async {
                self.listTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                self.filterContentForSearchText()
            }
        }
    }
}

extension ListViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        guard let searchText = searchBar.text else {return}
        albumPage = 1
        artistPage = 1
        trackPage = 1
        loadMore = true
        searchForContent(searchString: searchText, scope: scope)
    }
}

//MARK:- UITableViewDelegate & Datasource Methods
//MARK:-
extension ListViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell") as? ItemTableViewCell
        cell?.item = items[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row + 1 == items.count {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            spinner.tag = 1001
            self.listTableView.tableFooterView = spinner
            self.listTableView.tableFooterView?.isHidden = false
            if loadMore {
                switch seachType {
                case .album:
                    albumPage = albumPage + 1
                    guard let string = searchController.searchBar.text else {return}
                    self.fetchAlbum(searchFor: string, completion: {[weak self] (albums) in
                        self?.listTableView.tableFooterView?.viewWithTag(1001)?.removeFromSuperview()
                        self?.listTableView.tableFooterView?.isHidden = true
                        guard let albums = albums else {
                            return }
                        self?.album.append(contentsOf: albums)
                        self?.updateContentForSearchText(item: albums)
                    })
                case .artist:
                    artistPage = artistPage + 1
                    guard let string = searchController.searchBar.text else {return}
                    self.fetchArtist(searchFor: string, completion: {[weak self] (artists) in
                        self?.listTableView.tableFooterView?.viewWithTag(1001)?.removeFromSuperview()
                        self?.listTableView.tableFooterView?.isHidden = true
                        guard let artists = artists else {
                            return }
                        self?.artist.append(contentsOf: artists)
                        self?.updateContentForSearchText(item: artists)
                    })
                case .track:
                    trackPage = trackPage + 1
                    guard let string = searchController.searchBar.text else {return}
                    self.fetchTrack(searchFor: string, completion: {[weak self] (tracks) in
                        self?.listTableView.tableFooterView?.viewWithTag(1001)?.removeFromSuperview()
                        self?.listTableView.tableFooterView?.isHidden = true
                        guard let tracks = tracks else {
                            return }
                        self?.track.append(contentsOf: tracks)
                        self?.updateContentForSearchText(item: tracks)
                    })
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedItem = items[indexPath.row]
        self.performSegue(withIdentifier: "toDetail", sender: self)
    }
    
}
