//
//  DetailViewController.swift
//  Testing
//
//  Created by Ranjith on 1/3/19.
//  Copyright © 2019 Jaba. All rights reserved.
//

import UIKit
import FSPagerView

class DetailViewController: UIViewController {
    @IBOutlet weak var imageCarousel: FSPagerView!{
        didSet {
            self.imageCarousel.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        }
    }
    @IBOutlet weak var firstTitle: UILabel!
    @IBOutlet weak var firstContent: UILabel!
    @IBOutlet weak var secondTitle: UILabel!
    @IBOutlet weak var secondContent: UILabel!
    @IBOutlet weak var thirdTitle: UILabel!
    @IBOutlet weak var thirdContent: UILabel!
    @IBOutlet weak var fourthTitle: UILabel!
    @IBOutlet weak var fourthContent: UILabel!
    @IBOutlet weak var secondStack: UIStackView!
    @IBOutlet weak var thirdStack: UIStackView!
    
    typealias Completion = (Item?) -> Void

    var item:Item? = nil
    var artWorks = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchInfo {[unowned self] (item) in
            guard let item = item else { return }
            self.item = item
            DispatchQueue.main.async {
                self.populateContent()
            }
        }
    }
    
    // MARK: - Private Methods
    fileprivate func populateContent() {
        self.firstTitle.text = self.item?.category.rawValue.capitalized
        self.firstContent.text = self.item?.name
        self.fourthTitle.text = ""

        switch (self.item?.category)! {
        case .album:
            self.secondTitle.text = "Artist"
            self.secondContent.text = self.item?.artist
            self.thirdStack.isHidden = true
        case .track:
            self.secondTitle.text = "Artist"
            self.secondContent.text = self.item?.artist
            self.thirdTitle.text = "Album"
            self.thirdContent.text = self.item?.album
        case .artist:
            self.secondStack.isHidden = true
            self.thirdStack.isHidden = true
        }
        if !(self.item?.summary.isEmpty)!{
            self.fourthTitle.text = "Summary"
            self.fourthContent.text = self.item?.summary
        }
        artWorks = getLastTwo(myArray: self.item?.artWorks ?? []) ?? []
        self.imageCarousel.reloadData()
    }
    
    func getLastTwo(myArray:[String]) -> [String]? {
        return myArray.count >= 2 ? [myArray[myArray.count-2], myArray[myArray.count-1]] : nil
    }

    // MARK: - API
    
    func fetchInfo(completion:@escaping Completion) {
        self.startActivityIndicator()
        APIManager.fetchInfo(item: item!) { (success, response) in
            self.stopActivityIndicator()
            if success
            {
                guard let searchType = self.item?.category else {
                    completion(nil)
                    return
                }
                 let itemInfo = Item.init(json:response[searchType.rawValue],searchCategory:searchType)
                completion(itemInfo)
            }else{
                completion(nil)
            }
        }
    }
}

extension DetailViewController:FSPagerViewDelegate,FSPagerViewDataSource{
    // MARK: - FSPagerViewDataSource

    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return artWorks.count
    }

    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.contentMode = .scaleAspectFit

        var urlString = artWorks[index]
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!

        var photoUrl:URL? = nil
        if urlString != nil
        {
            photoUrl = URL.init(string:urlString)
            
        }

        if let url = photoUrl {
           cell.imageView?.sd_setImage(with: url , placeholderImage:UIImage(named: item?.category.rawValue ?? "album"))
        }

        return cell
    }

    // MARK: - FSPagerViewDelegate

    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
}
