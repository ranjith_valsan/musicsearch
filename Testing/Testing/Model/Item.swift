//
//  Item.swift
//  Testing
//
//  Created by Ranjith on 12/12/18.
//  Copyright © 2018 Jaba. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Item {
    var name:String! = ""
    var artWorkUrl:String! = ""
    var artist:String! = ""
    var album:String! = ""
    var summary:String! = ""
    var category : SearchType
    var artWorks:[String] = []
    
    init(json:JSON,searchCategory:SearchType){
         self.name = json["name"].string ?? ""
        if (json["artist"].string != nil){
            self.artist = json["artist"].string ?? ""
        }else{
            self.artist = json["artist"]["name"].string ?? ""

        }
        
        if ((json["wiki"]).dictionary != nil){
            self.summary = json["wiki"]["summary"].string ?? ""
        }else{
            self.summary = json["bio"]["summary"].string ?? ""
        }
         self.album = json["album"]["title"].string ?? ""
        self.artWorkUrl = json["image"][1]["#text"].stringValue 
        self.category = searchCategory
        
        if ((json["image"].array) != nil){
          artWorks = json["image"].arrayValue.map({$0["#text"].stringValue })
        }else{
          artWorks = json["album"]["image"].arrayValue.map({$0["#text"].stringValue })
        }

    }
}

